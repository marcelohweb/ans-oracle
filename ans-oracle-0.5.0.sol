pragma solidity ^0.4.0;

contract ANSOracle {

    /**
     * struct to represent an Entity
     */
    struct Entity{
        uint64 index;
        string entity;
        string ansCertificateCID;
    }

    /**
     * mapping to store associations. The key is an index (eg: cpf or cnpj), the value is an Entity struct
     */
    mapping(address => Entity) public addressToEntity;

    /**
     * Array to store all address registered. TODO Check if it is usable
     */
    address[] public entityAccts;

    event EntityRequest(address _address);
    event AddressRegistered(string _entityDescription);

    /**
     * ANS Listerner is listening this function
     */
    function searchEntityByAddress(address _address) public returns (string memory) {
        emit EntityRequest(_address);
        return "sent";
    }

    /**
     * Return an entity by an address
     */
    function getEntityByAddress(address _address) view public returns (uint, string memory, string memory){
        return (addressToEntity[_address].index, addressToEntity[_address].entity, addressToEntity[_address].ansCertificateCID);
    }

    /**
     * Function to register an association
     */
    function register(address _address, uint64 _index, string memory _entityDescription, string memory _ansCertificateCID) public returns (string memory){
        Entity storage entity = addressToEntity[_address];

        entity.index = _index;
        entity.entity = _entityDescription;
        entity.ansCertificateCID = _ansCertificateCID;

        entityAccts.push(_address) -1;

        emit AddressRegistered(_entityDescription);
        return _entityDescription;
    }

    /**
     * Return an entity index by an address
     */
    function getEntityIndex(address _address) view public returns (uint64){
        return addressToEntity[_address].index;
    }

    /**
     * Returns the entityAccts array
     */
    function getEntityAddress() view public returns(address[] memory){
        return entityAccts;
    }

}
