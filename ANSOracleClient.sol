pragma solidity ^0.5.0;

import './ANSOracle.sol';

contract ANSOracleClient {

    ANSOracle oracle;

    constructor(ANSOracle _oracle) public{

        oracle = ANSOracle(_oracle);

    }

    /**
     * Returns an entity CNPJ by an address
     */
    function getEntityIndex(address _address) view public returns (uint64){
        uint64 cnpj = oracle.getEntityIndex(_address);
        return cnpj;
    }

}
