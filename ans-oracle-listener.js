/*
ANS Oracle
*/

let Web3 = require('web3')
let fs = require('fs')
let request = require('request')

// using a Websockets URL.  Ganache works with Web3 1.0 and Websockets
const testNetWS = "ws://127.0.0.1:8546"

// create a WebsocketProvider
console.log("connecting to network")
const web3 = new Web3(new Web3.providers.WebsocketProvider(testNetWS))

console.log("isConnected=");
web3.eth.net.isListening().then(console.log);

// account address
const account = "0x67004379310f41145608631a5249f7228a27cf40"

// contract filename and the name of the contract itself
const filename = "ans-oracle.sol"
const contractName = ":ANSOracle"

// store the ABI for the contract so we can use it later
const abi = loadABI(filename, contractName)

// cut and paste the deployed contract address
const address = "0x95378fa659c2AD91Eb21D9b817a4FbfcD3c45396";

// listener contract

console.log("contract address: " + address)
startListener(address)

// starts the event listener
function startListener(address) {
    console.log("starting event monitoring on contract: " + address)
    let myContract = new web3.eth.Contract(abi, address);
    myContract.events.EntityRequest({fromBlock: 0, toBlock: 'latest'
    }, function(error, event){ console.log(">>> " + event) })
        .on('data', (log) => {
            console.log("event data: " + JSON.stringify(log, undefined, 2))
            console.log("param: " + log.returnValues._address)
            handler(address, log.returnValues._address)
        })
        .on('changed', (log) => {
            console.log(`Changed: ${log}`)
        })
        .on('error', (log) => {
            console.log(`error:  ${log}`)
        })
}

// handles a request event and sends the response to the contract
function handler(address, addressToSearch) {
    // using the excellent Dark Sky API - getting the forecast, centered on LA
    let url = "http://localhost:8084/get-entity-by-address-oracle/Ethereum/" + addressToSearch
    request(url, function(error, response, body) {
        if(error)
            console.log("error: " + error)

        console.log("status code: " + response.statusCode)
        let wx = JSON.parse(body)
        console.log(wx)
        let entity = wx.owner
        let arr = entity.split(":")

        //console.log("name: " + arr[0])
        //console.log("cpf_cnpj: " + arr[1])

        register(address, addressToSearch,  arr[1],  arr[0], wx.ansCertificateCID)

    })
}

async function register(contractAddress, addressToSearch, cpf_cnpj, name, ansCertificateCID, ansCertificateHash) {

  // run the 'register' method on the contract
  let MyContract = new web3.eth.Contract(abi, contractAddress);
  MyContract.options.from = account
  MyContract.options.gas = 1000000
  let k = MyContract.methods.register(addressToSearch, cpf_cnpj, name, ansCertificateCID).send()
      .then(function(result) {
          console.log("EVM call result: " + JSON.stringify(result, undefined, 2))
          //console.log("EVM call result: " + result)
          if(result.status == "0x1")
            console.log("+++transaction success+++")
          else if(result.status == "0x0")
            console.log("transaction failed!")
      }, function(error) {
          console.log("error "  + error)
      })

  await k

}

// Load, Compile and Deploy the contract
// returns a contract address
async function loadCompileDeploy(filename, contractName) {
    const solc = require('solc')
    let code = fs.readFileSync(filename).toString('utf-8')

    // compile
    console.log('compiling contract ' + filename)
    let output = solc.compile(code, 1)
    //console.log('output => ' + JSON.stringify(output, undefined, 2))
    let bc = output.contracts[contractName].bytecode
    abi = JSON.parse(output.contracts[contractName].interface)
    let contract = new web3.eth.Contract(abi)
    let addr = ""

    // deploy
    let x = contract.deploy({
        data: '0x' + bc
    }).send({
        from: account,
        gas: 555555
    }, function(error, transactionHash) {
        if(error) {
            console.log("error: " + error)
        }
        if(transactionHash)
            console.log("txId: " + transactionHash)
    }).on('receipt', function(receipt) {
        addr = receipt.contractAddress
    }).then(function(contractInstance){

    })

    // wait for it ...
    await x
    return addr

}

function loadABI(filename, contractName) {

    const solc = require('solc')
    let code = fs.readFileSync(filename).toString('utf-8')

    // compile
    console.log('compiling contract ' + filename)
    let output = solc.compile(code, 1)

    // return the ABI
    return JSON.parse(output.contracts[contractName].interface)
}

function sleep (time) {
    return new Promise((resolve) => setTimeout(resolve, time));
}
