pragma solidity ^0.4.21;

// Address Name System Oracle contract //
contract ANSOracle {

    struct Entity{
        uint cpf_cnpj;
        string name;
        string ansCertificateUrl;
        string ansCertificateHash;
    }

    mapping(uint => address) public entityToAddress;
    mapping(address => Entity) public addressToEntity;
    address[] public entityAccts;

    event AddressRequest(string entity);
    event EntityRequest(address _address);
    event FilledRequest(string rtemp);

    string public temp;

    function constructor() public {

    }

    function searchAddressByEntity(string entity) public returns (string) {
        emit AddressRequest(entity);
        return "sent";
    }

    function searchEntityByAddress(address _address) public returns (string) {
        emit EntityRequest(_address);
        return "sent";
    }

    /**
     * Return an entity by an address
     */
    function getEntityByAddress(address _address) view public returns (uint, string, string, string){
        return (addressToEntity[_address].cpf_cnpj, addressToEntity[_address].name, addressToEntity[_address].ansCertificateUrl, addressToEntity[_address].ansCertificateHash);
    }

    function fill(string entity, address _address) public returns (string){
        temp = entity;
        emit FilledRequest(entity);
        return temp;
    }

    function register(address _address, uint cpf_cnpj, string name, string ansCertificateUrl, string ansCertificateHash) public returns (string){
        temp = name;

        var entity = addressToEntity[_address];

        entity.cpf_cnpj = cpf_cnpj;
        entity.name = name;
        entity.ansCertificateUrl = ansCertificateUrl;
        entity.ansCertificateHash = ansCertificateHash;

        entityAccts.push(_address) -1;

        emit FilledRequest(name);
        return temp;
    }

    function getEntityAddress() view public returns(address[]){
        return entityAccts;
    }

    function countEntityAddress() view public returns (uint){
        return entityAccts.length;
    }

}
