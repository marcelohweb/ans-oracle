let Web3 = require('web3')
let fs = require('fs')
let request = require('request')

// using a Websockets URL.  Ganache works with Web3 1.0 and Websockets
const testNetWS = "ws://127.0.0.1:8546"

// create a WebsocketProvider
console.log("connecting to network")
const web3 = new Web3(new Web3.providers.WebsocketProvider(testNetWS))

console.log("isConnected=");
web3.eth.net.isListening().then(console.log);

// account address
const account = "0x67004379310f41145608631a5249f7228a27cf40"

// contract filename and the name of the contract itself
const filename = "ans-oracle.sol"
const contractName = ':ANSOracle'

// compile the contract and get the ABI
const abi = loadABI(filename, contractName)

// cut and paste the deployed contract address
const address = "0x95378fa659c2AD91Eb21D9b817a4FbfcD3c45396"

// run the request method on the contract
console.log("requesting address from oracle")
callContractForRequest(address)
    .then(function(result) {
        console.log("result: " + result)
    }, function(error) {
        console.log("oops! " + error)
    }

)


async function callContractForRequest(addr) {
    console.log("calling address: " + addr)

    let MyContract = new web3.eth.Contract(abi, addr);
    MyContract.options.from = account
    MyContract.options.gas = 100000
    let k = MyContract.methods.searchEntityByAddress("0xCD2a3d9F938E13CD947Ec05AbC7FE734Df8DD826").send()
        .then(function(result) {
            console.log("EVM call to request - got back: " + JSON.stringify(result))
        }, function(error) {
            console.log("error "  + error)
        })
    await k
    return "much success!"
}

function loadABI(filename, contractName) {

    const solc = require('solc')
    let code = fs.readFileSync(filename).toString('utf-8')

    // compile
    console.log('compiling contract ' + filename)
    let output = solc.compile(code, 1)

    // return the ABI
    return JSON.parse(output.contracts[contractName].interface)
}

function sleep (time) {
    return new Promise((resolve) => setTimeout(resolve, time));
}
