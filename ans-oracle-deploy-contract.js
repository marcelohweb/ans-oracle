/*
ANS Oracle
*/

let Web3 = require('web3')
let fs = require('fs')
let request = require('request')

// using a Websockets URL.  Ganache works with Web3 1.0 and Websockets
const testNetWS = "ws://127.0.0.1:8546"

// create a WebsocketProvider
console.log("connecting to network")
const web3 = new Web3(new Web3.providers.WebsocketProvider(testNetWS))

console.log("isConnected=");
web3.eth.net.isListening().then(console.log);

// account address
const account = "0x67004379310f41145608631a5249f7228a27cf40"

// store the ABI for the contract so we can use it later
var abi



// load, compile and deploy the included contract (contract.sol)
let c = loadCompileDeploy("ans-oracle.sol", ":ANSOracle").then(function(address) {
    console.log("contract address: " + address)
}, function(err) {
    console.log("shit didn't work.  here's why: " + err)
})

// Load, Compile and Deploy the contract
// returns a contract address
async function loadCompileDeploy(filename, contractName) {
    const solc = require('solc')
    let code = fs.readFileSync(filename).toString('utf-8')

    // compile
    console.log('compiling contract ' + filename)
    let output = solc.compile(code, 1)
    //console.log('output => ' + JSON.stringify(output, undefined, 2))
    let bc = output.contracts[contractName].bytecode
    abi = JSON.parse(output.contracts[contractName].interface)
    let contract = new web3.eth.Contract(abi)
    let addr = ""

    // deploy
    let x = contract.deploy({
        data: '0x' + bc
    }).send({
        from: account,
        gas: 2000000
    }, function(error, transactionHash) {
        if(error) {
            console.log("error: " + error)
        }
        if(transactionHash)
            console.log("txId: " + transactionHash)
    }).on('receipt', function(receipt) {
        addr = receipt.contractAddress
    }).then(function(contractInstance){

    })

    // wait for it ...
    await x
    return addr

}

function sleep (time) {
    return new Promise((resolve) => setTimeout(resolve, time));
}
